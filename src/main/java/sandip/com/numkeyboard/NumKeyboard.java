package sandip.com.numkeyboard;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.Dimension;
import android.support.annotation.DrawableRes;
import android.support.annotation.FontRes;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatDelegate;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class NumKeyboard extends ConstraintLayout{

    private static final int DEFAULT_KEY_WIDTH_DP = -1; //match_parent
    private static final int DEFAULT_KEY_HEIGHT_DP = -1; //match_parent
    private static final int DEFAULT_KEY_PADDING_DP = 16; //match_parent;

    static {

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Dimension
    private int keyWidth;
    @Dimension
    private int keyHeight;
    @Dimension
    private int keyPadding;
    @DrawableRes
    private int keyBackground;
    @DrawableRes
    private int keyTextColor;
    @Dimension
    private int keyFontSize;
    @DrawableRes
    private int leftAuxKeyIcon;
    @DrawableRes
    private int leftAuxKeyBackground;
    @DrawableRes
    private int rightAuxKeyIcon;
    @DrawableRes
    private int rightAuxKeyBackground;

    List<Button> keys;
    ImageView leftAuxKey;
    ImageView rightAuxKey;

    NumKeyListener listener;

    public NumKeyboard(Context context) {
        super(context);

        inflateView();
    }

    public NumKeyboard(Context context, AttributeSet attrs) {
        super(context, attrs);

        initAttr(attrs);
        inflateView();
    }

    public NumKeyboard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initAttr(attrs);
        inflateView();
    }

    public void setKeyWidth(int keyWidth) {

        if(keyWidth == DEFAULT_KEY_WIDTH_DP){
            return;
        }

        for(Button key : keys){

            key.getLayoutParams().width = keyWidth;
        }

        leftAuxKey.getLayoutParams().width = keyWidth;
        rightAuxKey.getLayoutParams().height = keyWidth;

        requestLayout();
    }

    public void setKeyHeight(int keyHeight) {

        if(keyHeight == DEFAULT_KEY_HEIGHT_DP){
            return;
        }

        for(Button key : keys){

            key.getLayoutParams().height = keyHeight;
        }

        leftAuxKey.getLayoutParams().height = keyHeight;
        rightAuxKey.getLayoutParams().height = keyHeight;

        requestLayout();

    }

    public void setKeyPadding(int keyPadding) {

        if(keyPadding == DEFAULT_KEY_PADDING_DP){
            return;
        }

        for(Button key : keys){

            key.setPadding(keyPadding,keyPadding,keyPadding,keyPadding);
        }

        requestLayout();
    }
    public void setKeyBackground(int keyBackground) {
        for(Button key : keys){

            key.setBackgroundResource(keyBackground);
        }

        requestLayout();
    }

    public void setKeyTextColor(int keyTextColor) {

        for(Button key : keys){

            key.setTextColor(keyTextColor);
        }

        requestLayout();
    }

    public void setKeyFontSize(int keyFontSize) {

        for(Button key : keys){

            key.setTextSize(keyFontSize);
        }

        requestLayout();
    }

    public void setLeftAuxKeyIcon(int leftAutKeyIcon) {

        this.leftAuxKey.setImageResource(leftAutKeyIcon);
    }

    public void setLeftAuxKeyBackground(int leftAuxKeyBackground) {

        this.leftAuxKey.setBackgroundResource(leftAuxKeyBackground);
    }

    public void setRightAuxKeyIcon(int rightAuxKeyIcon) {

        this.rightAuxKey.setImageResource(rightAuxKeyIcon);
    }

    public void setRightAuxKeyBackground(int rightAuxKeyBackground) {

        this.rightAuxKey.setBackgroundResource(rightAuxKeyBackground);
    }

    private void initAttr(AttributeSet attrs){

        TypedArray array = getContext().getTheme().obtainStyledAttributes(attrs,R.styleable.NumKeyboard,0,0);

        try{

            // Get key sizes
            keyWidth = array.getLayoutDimension(R.styleable.NumKeyboard_keyWidth, DEFAULT_KEY_WIDTH_DP);
            keyHeight = array.getLayoutDimension(R.styleable.NumKeyboard_height, DEFAULT_KEY_HEIGHT_DP);
            // Get key padding
            keyPadding = array.getDimensionPixelSize(R.styleable.NumKeyboard_keyPadding,
                    dpToPx(DEFAULT_KEY_PADDING_DP));
            // Get number key background
            keyBackground = array.getResourceId(R.styleable.NumKeyboard_keyBackground, R.drawable.key_background);
            // Get number key text color
            keyTextColor = array.getResourceId(R.styleable.NumKeyboard_keyTextColor,
                    R.color.keyTextColor);
        }finally {
            array.recycle();
        }

    }

    private void inflateView(){

        View view = inflate(getContext(),R.layout.keyboard,this);
        keys = new ArrayList<>();
        keys.add((Button) view.findViewById(R.id.key0));
        keys.add((Button) view.findViewById(R.id.key1));
        keys.add((Button) view.findViewById(R.id.key2));
        keys.add((Button) view.findViewById(R.id.key3));
        keys.add((Button) view.findViewById(R.id.key4));
        keys.add((Button) view.findViewById(R.id.key5));
        keys.add((Button) view.findViewById(R.id.key6));
        keys.add((Button) view.findViewById(R.id.key7));
        keys.add((Button) view.findViewById(R.id.key8));
        keys.add((Button) view.findViewById(R.id.key9));

        leftAuxKey = (ImageView) view.findViewById(R.id.leftAuxBtn);
        rightAuxKey = (ImageView) view.findViewById(R.id.rightAuxBtn);

        setStyles();
        setupListeners();
    }

    private void setStyles(){

        setKeyWidth(keyWidth);
        setKeyHeight(keyHeight);
        setKeyPadding(keyPadding);
        setKeyBackground(keyBackground);
        setKeyTextColor(keyTextColor);
        setKeyFontSize(keyFontSize);
        setLeftAuxKeyIcon(leftAuxKeyIcon);
        setLeftAuxKeyBackground(leftAuxKeyBackground);
        setRightAuxKeyIcon(rightAuxKeyIcon);
        setRightAuxKeyBackground(rightAuxKeyBackground);
    }

    private void setupListeners() {
        // Set number callbacks
        for (int i = 0; i < keys.size(); i++) {
            final Button key = keys.get(i);
            final int number = i;
            key.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listener != null) {
                        listener.onKeyListener(number);
                    }
                }
            });
        }
        // Set auxiliary key callbacks
        leftAuxKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onLeftAuxKeyListener();
                }
            }
        });
        rightAuxKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onRightAuxKeyListener();
                }
            }
        });
    }


    public int dpToPx(float valueInDp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, getResources().getDisplayMetrics());
    }
}
