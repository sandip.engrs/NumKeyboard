package sandip.com.numkeyboard;

public interface NumKeyListener {

    void onKeyListener(int key);

    void onLeftAuxKeyListener();

    void onRightAuxKeyListener();

}
